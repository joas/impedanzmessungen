from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Using cell impedance data to investigate cell migration',
    author='Max Joas',
    license='MIT',
)
