import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import click

from src.settings import FEATURES, FILES_VERSION
import sys


def plot_pca(plt_df, style_col):
    """ Plot PCA components
        Params:
            plt_df: dataframe with pca components and labels
            style_col: column to use for style
        Returns:
            None

    """
    sns.scatterplot(x="PC1", y="PC2", hue="coverage",
                    data=plt_df, legend='brief')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.savefig(
        f"reports/figures/pca_{FEATURES}_{FILES_VERSION}.png", bbox_inches="tight")
    plt.close()

    fig, ax = plt.subplots(1, 2, figsize=(10, 6), constrained_layout=True)
    sns.scatterplot(x="PC1", y="PC2", hue="coverage",
                    data=plt_df, legend='brief', ax=ax[0])
    sns.scatterplot(x="PC1", y="PC2", hue=str(style_col),
                    data=plt_df, legend='brief', ax=ax[1])
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.savefig(
        f"reports/figures/pca_{FEATURES}_{FILES_VERSION}_compare_{style_col}.png", bbox_inches="tight")
    plt.close()

    sns.scatterplot(x="PC1", y="PC2", hue="coverage", style=str(style_col),
                    data=plt_df, legend='brief')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.savefig(
        f"reports/figures/pca_{style_col}_{FEATURES}_{FILES_VERSION}.png", bbox_inches="tight")
    plt.close()

    sns.scatterplot(x="PC1", y="PC2", hue="coverage", size='max',
                    data=plt_df, legend='brief')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.savefig(
        f"reports/figures/pca_max_size_{FEATURES}_{FILES_VERSION}.png", bbox_inches="tight")
    plt.close()


def plot_latent_space(plt_df, style_col):
    """ Generates a scatter plot of the latent space """
    sns.scatterplot(x="latent1", y="latent2", hue="coverage",
                    data=plt_df, legend='brief')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.savefig(
        f"reports/figures/latent_space_{FEATURES}_{FILES_VERSION}.png", bbox_inches="tight")
    plt.close()

    fig, ax = plt.subplots(1, 2, figsize=(10, 6), constrained_layout=True)
    sns.scatterplot(x="latent1", y="latent2", hue="coverage",
                    data=plt_df, legend='brief', ax=ax[0])
    sns.scatterplot(x="latent1", y="latent2", hue=str(
        style_col), data=plt_df, legend='brief', ax=ax[1])
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.savefig(
        f"reports/figures/latent_space_{FEATURES}_{FILES_VERSION}_{style_col}_compare.png", bbox_inches="tight")
    plt.close()

    # with max impedance as size in scatterplot
    sns.scatterplot(x="latent1", y="latent2", hue="coverage", style=str(style_col),
                    data=plt_df, legend='brief')
    # label outside plot=
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.savefig(
        f"reports/figures/latent_space_{style_col}_{FEATURES}_{FILES_VERSION}_{style_col}.png", bbox_inches="tight")
    plt.close()

    sns.scatterplot(x="latent1", y="latent2", hue="coverage", size='max',
                    legend='brief', data=plt_df)
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.savefig(
        f"reports/figures/latent_space_max_size_{FEATURES}_{FILES_VERSION}.png", bbox_inches="tight")
    plt.close()


def plot_comparison(original, reconstructed, df_features):
    """ Creates plot of mean, max, min of original and reconstructed data

    Params:
        original (torch.Tensor): original data
        reconstructed (torch.Tensor): reconstructed data
        df_features (pd.DataFrame): dataframe of features used for colnames
    Returns:
        Nonea

    """
    reconstructed = pd.DataFrame(
        reconstructed.detach().numpy(), columns=df_features.columns)
    original = pd.DataFrame(original.detach().numpy(),
                            columns=df_features.columns)

    plot_helper = [(original.max(axis=0), reconstructed.max(axis=0)),
                   (original.min(axis=0), reconstructed.min(axis=0)),
                   (original.mean(axis=0), reconstructed.mean(axis=0))]
    notations = ["max", "min", "mean"]

    fig, ax = plt.subplots(3, 1, figsize=(10, 6), constrained_layout=True)
    for i, (original, reconstructed) in enumerate(plot_helper):
        ax[i].plot(original, label="original")
        ax[i].plot(reconstructed, label="reconstructed")
        ax[i].set_title(f"Original vs reconstructed {notations[i]}")
        # log scale x axis
        ax[i].set_xscale("log")
        ax[i].legend()
        ax[i].set_xlabel("Frequency")
        ax[i].set_ylabel("Impedance")

    plt.savefig(
        f"reports/figures/ae_reconstruction_{FEATURES}_{FILES_VERSION}.png", bbox_inches="tight")
    plt.close()


def plot_tsne(plt_df):
    """ Plot TSNE
        Params:
            plt_df: dataframe with tsne components and labels
        Returns:
            None
    """

    sns.scatterplot(x="TSNE1", y="TSNE2", hue="coverage",
                    data=plt_df, legend='brief')
    plt.savefig(
        f"reports/figures/tsne_{FEATURES}_{FILES_VERSION}.png", bbox_inches="tight")
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.close()

    sns.scatterplot(x="TSNE1", y="TSNE2", hue="coverage", style="max_quantile",
                    data=plt_df, legend='brief')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.savefig(
        f"reports/figures/tsne_max_markers_{FEATURES}_{FILES_VERSION}.png", bbox_inches="tight")
    plt.close()

    sns.scatterplot(x="TSNE1", y="TSNE2", hue="coverage", size='max',
                    data=plt_df, legend='brief')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.savefig(
        f"reports/figures/tsne_max_size_{FEATURES}_{FILES_VERSION}.png", bbox_inches="tight")
    plt.close()


def plot_pca_variance(exp_var_pca):
    """ Plot PCA variance """

    cum_sum_eigenvalues = np.cumsum(exp_var_pca)
    plt.bar(range(0, len(exp_var_pca)), exp_var_pca, alpha=0.5, align='center',
            label='Individual explained variance')
    plt.step(range(0, len(cum_sum_eigenvalues)), cum_sum_eigenvalues,
             where='mid', label='Cumulative explained variance')
    plt.ylabel('Explained variance ratio')
    plt.xlabel('Principal component index')
    plt.legend(loc='best')
    plt.savefig(
        f"reports/figures/pca_variance_{FEATURES}_{FILES_VERSION}.png", bbox_inches="tight")
    plt.close()


# add a column to the dataframe that gives either the prediction of df_rf or if
# the index does not match give the value of the coverage column
def add_rf_prediction(df, df_rf):
    """ Adds rf prediction to dataframe

    Params:
        df (pd.DataFrame): dataframe with coverage column
        df_rf (pd.DataFrame): dataframe with rf predictions
    Returns:
        df (pd.DataFrame): dataframe with coverage and rf prediction column
    """
    df["prediction"] = df.index.map(df_rf["preds"].to_dict())
    df["prediction"] = df["prediction"].fillna('training_data')
    # if the prediction is equal to the coverage column, then the prediction is correct
    # in this case we set the value to 'correct' else we set it to the value of
    # th predictionw
    df["prediction"] = np.where(
        df["prediction"] == df["coverage"], 'correct', df["prediction"])

    return df


@click.command()
@click.argument("infile", type=click.Path(exists=True))
def main(infile):
    df_all = pd.read_csv(
        f"data/processed/{FEATURES}_{FILES_VERSION}_tsne.csv", index_col=0)

    df_pred = pd.read_csv(infile, index_col=0)
    df = add_rf_prediction(df_all, df_pred)
    plot_pca(df, 'prediction')
    plot_latent_space(df, 'prediction')


if __name__ == "__main__":
    main()
