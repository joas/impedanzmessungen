import logging

FILES_VERSION = '2'
FEATURES = 'betrag' # chose from 'p_times_b', 'betrag' and 'phasenwinkel'
LEERWERT_START_INDEX = 0
LEERWERT_STOP_INDEX = 51
PHASENWINKEL_START_INDEX = 53
PHASENWINKEL_STOP_INDEX = 104
LAST_COL_EMPTY = False
DELETE_FIRST_ROW = True
LABEL = 'coverage'
SKIPROWS = 8
FEATURE_INDEX = -3 # index how to slice whole df (including metadata) to only get features
log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logging.basicConfig(level=logging.INFO, format=log_fmt)
