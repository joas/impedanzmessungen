# import torch dataset
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import torch
from torch.utils.data import Dataset

from src.settings import LABEL, FEATURE_INDEX

class ImpedanceDataset(Dataset):
    def __init__(self, file_name, transform=None):
        df = pd.read_csv(file_name)
        self.data = df.iloc[:, 0:FEATURE_INDEX]
        le = LabelEncoder()
        labels = le.fit_transform(df[LABEL])
        self.transform = transform

        self.x = torch.tensor(self.data.values, dtype=torch.float32)
        self.y = torch.tensor(labels, dtype=torch.int64)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        sample = self.x[idx]
        label = self.y[idx]
        if self.transform:
            sample = self.transform(sample)
        return sample, label

    def shape(self):
        return self.x.shape
