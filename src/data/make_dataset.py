# -*- coding: utf-8 -*-
import logging
import os
from pathlib import Path

import click
from dotenv import find_dotenv, load_dotenv
import numpy as np
import pandas as pd

from src.settings import (
    DELETE_FIRST_ROW,
    FILES_VERSION,
    LAST_COL_EMPTY,
    LEERWERT_START_INDEX,
    LEERWERT_STOP_INDEX,
    PHASENWINKEL_START_INDEX,
    PHASENWINKEL_STOP_INDEX,
    SKIPROWS,
)

logger = logging.getLogger(__name__)


def get_na_dict(data):
    """Returns a dictionary with the number of NaN values per column
    Params:
        data: pandas dataframe
    Returns:
        dictionary with column names as keys and number of NaN values as values
    """
    na_dict = {}

    for col in data.columns:
        na_value = data[col].isna().mean()
        na_dict[col] = na_value
    # sort na_dict by value
    na_dict = {k: v for k, v in sorted(
        na_dict.items(), key=lambda item: item[1])}
    return na_dict


def calc_relative_impedance(data, data_leer, measurement_type="betrag"):
    """Calculates the relative impedance from the data and phasenwinkel and
    aggregates data with mean
    Params:
        data: pandas dataframe with data values
        phasenwinkel: pandas dataframe with phasenwinkel values
        measurement_type: str, either "betrag" or "phasenwinkel"
    Returns:
        data_rel: pandas dataframe with relative impedance values
    """
    data2 = data.reset_index()
    data2_leer = data_leer.reset_index()
    # data2 = data2.drop("Frequenz", axis=1)
    data2_leer = data2_leer.drop(data2_leer.columns[0], axis=1)

    data_rel = ((data2 - data2_leer) / data2_leer) * 100
    if measurement_type == "phasenwinkel":
        data_rel = data_rel = data2 - data2_leer
    data_rel.index = data.index
    return data_rel.mean(axis=1)


def clean_data(in_path):
    """Cleans data and gets betrag and phasenwinkel
    Params:
        in_path: str, path to csv file
    Returns:
        betrag: pandas dataframe
        phasenwinkel: pandas dataframe

    """
    # in_path = "data/raw/emptyBeads_1/11.30.2022_08.24.38_24h-MI_A062.csv"
    data = pd.read_csv(filepath_or_buffer=in_path, sep=";", skiprows=SKIPROWS, index_col=1)
    # removes last column if the column is empty
    if LAST_COL_EMPTY:
        data = data.drop(data.columns[[-1, 0]], axis=1)
    # first row is usually filled with timestamp data that we don't need
    if DELETE_FIRST_ROW:
        data = data.drop(data.index[0], axis=0)
    data = data.replace(",", ".", regex=True)  # transforming to EN decimals
    data.index = data.index.str.replace(',', '.')
    data.head()
    # data.index = data.iloc[:, 1]  # making frequency index column
    na_dict = get_na_dict(data)
    logger.debug(f"na values per column: {na_dict}")
    # drop frequency column (is index now)
    data = data.drop(data.columns[0:2], axis=1)
    logger.debug(f"data shape: {data.shape}")
    data = data.dropna(axis=1, how="any")
    data.replace(" ", np.nan, inplace=True)  # for empty column in between
    data = data.dropna(axis=1, how="any")
    logger.debug(f"data shape after dropping na values: {data.shape}")

    betrag = data.iloc[LEERWERT_START_INDEX:LEERWERT_STOP_INDEX, :].astype(
        float
    )  # betrag
    phasenwinkel = data.iloc[
        PHASENWINKEL_START_INDEX:PHASENWINKEL_STOP_INDEX, :
    ].astype(
        float
    )  # Phase
    return betrag, phasenwinkel


def combine_csvs(all_files):
    """Combines all csv files in to one dataframe and stores metadata in extra
    column. The data is already cleaned and the relative impedance is
    calculated
    Params:
        all_files: dict, keys are the experiment names, values are lists of
        paths to csv files
    Returns:
        df: pandas dataframe with all data combined

    """
    # get first value of the all_files dict for col names
    first_value = list(all_files.values())[0][1][0]
    betrag, _ = clean_data(first_value)
    df = pd.DataFrame(columns=betrag.index)
    df_phasenwinkel = pd.DataFrame(columns=betrag.index)
    meta_cols = ["filename", "coverage", "experiment"]
    # add meta_cols to df
    for col in meta_cols:
        df[col] = ""
        df_phasenwinkel[col] = ""

    for key, _ in all_files.items():
        filelist = all_files[key][1]
        leerwert_path = all_files[key][0]
        betrag_leer, phasenwinkel_leer = clean_data(
            leerwert_path
        )
        for file in filelist:
            try:
                betrag, phasenwinkel = clean_data(file)
                betrag = calc_relative_impedance(betrag, betrag_leer)
                phasenwinkel = calc_relative_impedance(
                    phasenwinkel,
                    phasenwinkel_leer,
                    measurement_type="phasenwinkel"
                )
                betrag["filename"] = str(file)
                betrag["coverage"] = key.split("_")[0]
                betrag["experiment"] = key.split("_")[1]
                phasenwinkel["filename"] = str(file)
                phasenwinkel["coverage"] = key.split("_")[0]
                phasenwinkel["experiment"] = key.split("_")[1]

                df.loc[len(df.index)] = betrag
                df_phasenwinkel.loc[len(df_phasenwinkel.index)] = phasenwinkel
            except Exception as e:
                logger.warning(e)
                logger.warning("Error in file: ", file)
                raise e
    return df, df_phasenwinkel


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_filepath", type=click.Path())
def main(input_filepath, output_filepath):
    """Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """
    logger.info("making final data set from raw data")
    logger.info(output_filepath)

    # print each foldername in the path
    all_files = {}
    for folder in Path(input_filepath).iterdir():
        logger.debug(f"folder: {folder}")
        if not folder.is_dir():
            continue
        # get all csv files in that folder
        file_list = [
            os.path.join(
                input_filepath,
                folder.name,
                f.name,
            )
            for f in folder.glob("*.csv")
        ]
        # find the leerwert file
        leerwert_file = [f for f in file_list if "blank" in f.lower()]
        leerwert_file = leerwert_file[0]
        # remove leerwert file from file_list
        file_list.remove(leerwert_file)
        all_files[folder.name] = [leerwert_file, file_list]

    combined_betrag, combined_phasenwinkel = combine_csvs(all_files)
    # define filename based on timestamp
    filename = f"betrag_combined_{FILES_VERSION}.csv"
    combined_betrag.to_csv(os.path.join(
        output_filepath, filename), index=False)
    logger.info(f"saved combined data to {output_filepath}")
    filename = f"phasenwinkel_combined_{FILES_VERSION}.csv"
    combined_phasenwinkel.to_csv(os.path.join(
        output_filepath, filename), index=False)
    logger.info(f"saved combined data to {output_filepath}")

    # drop meta columns from betrag and phasenwinkel data frame
    df_phasenwinkel = combined_betrag.drop(
        columns=["filename", "coverage", "experiment"])
    df_betrag = combined_phasenwinkel.drop(
        columns=["filename", "coverage", "experiment"])
    df_combined = df_phasenwinkel * df_betrag

    df_combined["filename"] = combined_betrag["filename"]
    df_combined["coverage"] = combined_betrag["coverage"]
    df_combined["experiment"] = combined_betrag["experiment"]

    filename = f"p_times_b_combined{FILES_VERSION}.csv"
    df_combined.to_csv(os.path.join(output_filepath, filename), index=False)

    logger.info(f"saved combined data to {output_filepath}")
    logger.info('done1')


if __name__ == "__main__":

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())
    main()
