import logging

import pandas as pd
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
import torch

from src.settings import FEATURES, FILES_VERSION, LABEL, FEATURE_INDEX
from src.features.build_features import ImpedanceDataset
from src.models.train_model import Autoencoder
from src.visualization.visualize import (
    plot_comparison,
    plot_latent_space,
    plot_pca,
    plot_pca_variance,
    plot_tsne,
)

# GLOBAL VARIABLES
df = pd.read_csv(f"data/processed/{FEATURES}_combined_{FILES_VERSION}.csv")
plt_df = df
df_features = df.iloc[:,0:FEATURE_INDEX]
df_features.columns = df_features.columns.astype(float)
df_label = df[LABEL]
targets = df_label.unique()
plt_df['max'] = df_features.max(axis=1)
plt_df['max_rank'] = plt_df['max'].rank(pct=True)
plt_df['max_quantile'] = pd.qcut(plt_df['max'], q=[0, .25, .75, 1],
                                 labels = ['<.25', '.25-.75', '>.75'])

logger = logging.getLogger(__name__)

def predict_latent_space(plt_df):
    """ Predict latent space from trained model
        Params:
            plt_df: data frame with features and label
        Returns:
            None
    """

    my_ds = ImpedanceDataset(f"data/processed/{FEATURES}_combined_{FILES_VERSION}.csv")
    model =  Autoencoder(input_size=my_ds.shape()[1], hidden_size=2)
    model.load_state_dict(torch.load(f"models/autoencoder_model_{FILES_VERSION}.pt"))
    model.eval()

    original, _ = my_ds[:]
    latent = model.encoder(original)
    reconstructed = model.decoder(latent)
    # plot latent space
    latent = latent.detach().numpy()
    plt_df['latent1'] = latent[:,0]
    plt_df['latent2'] = latent[:,1]
    plot_latent_space(plt_df, 'max_quantile')
    labels = df_label.unique()
    logger.info(f'LABELS: {labels}')
    plot_comparison(original, reconstructed, df_features=df_features)
    plt_df.to_csv(f"data/processed/{FEATURES}_{FILES_VERSION}_latent.csv")
    logger.info(f'Latent space saved to {FEATURES}_{FILES_VERSION}_latent.csv')


def predict_pca(n_dim, plt_df):
    """ Predict PCA, plot and color cluster by label
        Params:
            n_dim: number of dimensions to reduce to
            plt_df: data frame with features and label
        Returns:
            None
    """
    x = StandardScaler().fit_transform(df_features)
    # PCA
    pca = PCA()
    pca_fit = pca.fit_transform(x)
    # get first component
    columns = [f"PC{i}" for i in range(1, n_dim+1)]
    for i in range(n_dim):
        plt_df[columns[i]] = pca_fit[:,i]

    projection_str = f'{n_dim}d'
    plot_pca(plt_df, 'max_quantile')

    exp_var_pca = pca.explained_variance_ratio_
    plot_pca_variance(exp_var_pca)
    plt_df.to_csv(f"data/processed/{FEATURES}_{FILES_VERSION}_pca.csv")
    logger.info(f'PCA saved to {FEATURES}_{FILES_VERSION}_pca.csv')

def predict_tsne(plt_df):
    """ Predict t-SNE, plot and color cluster by label
        Params:
            plt_df: data frame with features and label
        Returns:
            None

    """
    tsn_model = TSNE(n_components=2,
                    learning_rate='auto',
                    init='random', perplexity=3)
    X_embedded = tsn_model.fit_transform(df_features)
    plt_df['TSNE1'] = X_embedded[:,0]
    plt_df['TSNE2'] = X_embedded[:,1]
    plot_tsne(plt_df)
    plt_df.to_csv(f"data/processed/{FEATURES}_{FILES_VERSION}_tsne.csv")
    logger.info(f't-SNE saved to {FEATURES}_{FILES_VERSION}_tsne.csv')

if __name__ == "__main__":
    logger.info("Predicting latent space")
    predict_latent_space(plt_df)
    logger.info("Predicting PCA 2D")
    predict_pca(2, plt_df)
    logger.info("Predicting t-SNE")
    predict_tsne(plt_df)
