import logging

import torch
from torch import nn
from torch.utils.data import DataLoader

from src.settings import FEATURES, FILES_VERSION
from src.features.build_features import ImpedanceDataset
# TEMPORARY CONFIG
logger = logging.getLogger(__name__)
torch.Tensor.ndim = property(lambda self: len(self.shape))
torch.manual_seed(0)


# basic autoencoder
# vanilla autoencoder structure
class Autoencoder(nn.Module):
    """ Autoencoder class, basic implemenation with two layers """
    def __init__(self, input_size, hidden_size):
        super(Autoencoder, self).__init__()

        self.input_size = input_size
        self.hidden_size = hidden_size

        self.encoder = nn.Sequential(
            nn.Linear(input_size, 32),
            nn.Linear(32, hidden_size),
            nn.LeakyReLU()
        )
        self.decoder = nn.Sequential(
            nn.Linear(hidden_size, 32),
            nn.Linear(32, input_size),
            nn.LeakyReLU()
        )


    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x


# transform df_features into pytorch tensor
def train_ae_model():
    my_ds = ImpedanceDataset(f"data/processed/{FEATURES}_combined_{FILES_VERSION}.csv")
    # log shape of dataset
    logger.info(my_ds.shape())
    train_loader = DataLoader(my_ds, batch_size=len(my_ds), shuffle=False)
    model = Autoencoder(my_ds.shape()[1], 2)
    L = nn.MSELoss()
    opt = torch.optim.Adam(model.parameters(), lr=1e-3, weight_decay=1e-5)
    for epoch in range(5000):
        loss = 0
        for batch, _ in train_loader:
            opt.zero_grad()
            out = model(batch)
            batch_loss = L(out, batch)
            batch_loss.backward()
            opt.step()
            loss += batch_loss
        if epoch % 100 == 0:
            logger.info(f"Epoch: {epoch} Loss: {loss}")

    torch.save(model.state_dict(),
            f"models/autoencoder_model_{FILES_VERSION}.pt")
    logger.info(f"Model saved as autoencoder_model_{FILES_VERSION}.pt")


if __name__ == "__main__":
    train_ae_model()
    logger.info("Model trained and saved.")
